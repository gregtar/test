package com.company;

public class VehicleNotFoundException(
        public class VehicleNotFoundException extends Exception {
    public VehicleNotFoundException(String message) {
        super(message);
    }
}

public class CustomerAlreadyExistsException extends Exception {
    public CustomerAlreadyExistsException(String message) {
        super(message);
    }
}

public class CustomerNotFoundException extends Exception {
    public CustomerNotFoundException(String message) {
        super(message);
    }
}

public class IllegalCustomerException extends Exception {
    public IllegalCustomerException(String message) {
        super(message);
    }
}

public class InvalidPhoneNumberException extends Exception {
    public InvalidPhoneNumberException(String message) {
        super(message);
    }
}

public class IllegalVehicleException extends Exception {
    public IllegalVehicleException(String message) {
        super(message);
    }
}

public class InvalidNumberPlateException extends Exception {
    public InvalidNumberPlateException(String message) {
        super(message);
    }
}

public class VehicleAlreadyAssignedException extends Exception {
    public VehicleAlreadyAssignedException(String message) {
        super(message);
    }
 {
}
