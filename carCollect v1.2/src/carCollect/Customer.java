package carCollect;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;



public class Customer {
    private String id;
    private String name;
    private String surname;
    private String phone;
    private String birthDate;

    private HashMap<String, Vehicle> registeredVehicles;

    public Customer(String name, String surname, String phone, String birthDate) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.birthDate = birthDate;
        this.registeredVehicles = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhone() {
        return phone;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void addVehicle(Vehicle newVehicle) throws VehicleAlreadyAssignedException, IllegalVehicleException {
        if (newVehicle == null)
            throw new IllegalVehicleException("Provided vehicle is invalid");

        if (registeredVehicles.containsKey(newVehicle.getNumberPlate()))
            throw new VehicleAlreadyAssignedException("Vehicle with number plate " + newVehicle.getNumberPlate() +
                    " is already assigned for " + name + " " + surname);

        registeredVehicles.put(newVehicle.getNumberPlate(), newVehicle);
    }

    public void removeVehicle(Vehicle vehicle) throws IllegalVehicleException, VehicleNotFoundException {
        if (vehicle == null)
            throw new IllegalVehicleException("Provided vehicle is invalid");

        checkVehicleAssigned(vehicle.getNumberPlate());

        registeredVehicles.remove(vehicle.getNumberPlate());
    }

    public Vehicle findVehicle(String numberPlate) throws VehicleNotFoundException, InvalidNumberPlateException {
        if ((numberPlate == null) || numberPlate.isEmpty())
            throw new InvalidNumberPlateException("Number plate is invalid");

        checkVehicleAssigned(numberPlate);

        return registeredVehicles.get(numberPlate);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("--- User info ---\n");
        builder.append(primaryFieldsToString());

        for (Map.Entry<String, Vehicle> vehicleEntry : registeredVehicles.entrySet()) {
            builder.append("\n").append(vehicleEntry.getValue().toString());
        }
        builder.append("--- End ---\n");

        return builder.toString();
    }

    private String primaryFieldsToString() {
        return "Customer : " + id + "\n" +
                "  name : " + name + "\n" +
                "  surname : " + surname + "\n" +
                "  phone : " + phone + "\n" +
                "  date of birth: " + birthDate;
    }

    private void checkVehicleAssigned(String numberPlate) throws VehicleNotFoundException {
        if (!registeredVehicles.containsKey(numberPlate))
            throw new VehicleNotFoundException("Vehicle with number plate " + numberPlate +
                    " is not assigned for " + name + " " + surname);
    }
}