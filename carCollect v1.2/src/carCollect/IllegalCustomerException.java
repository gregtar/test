package carCollect;

public class IllegalCustomerException extends Exception {
    public IllegalCustomerException(String message) {
        super(message);
    }
}
