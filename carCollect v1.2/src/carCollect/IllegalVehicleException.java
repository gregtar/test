package carCollect;

public class IllegalVehicleException extends Exception {
    public IllegalVehicleException(String message) {
        super(message);
    }
}