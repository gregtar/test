package carCollect;


public class Main {
    public static void main(String[] args) {
        Workshop workshop = createWorkshop();

        System.out.println(workshop);

        removeMercedesFromPetrov(workshop);

        System.out.println(workshop);
    }

    private static void removeMercedesFromPetrov(Workshop workshop) {
        Customer customer = null;
        try {
            customer = workshop.findCustomerByPhoneNumber("+78005678930");
        } catch (InvalidPhoneNumberException e) {
            e.printStackTrace();
        } catch (CustomerNotFoundException e) {
            e.printStackTrace();
        }

        Vehicle vehicle = null;

        try {
            vehicle = customer.findVehicle("H174AK199"); // it will not be found
        } catch (VehicleNotFoundException e) {
            e.printStackTrace();
        } catch (InvalidNumberPlateException e) {
            e.printStackTrace();
        }

        try {
            vehicle = customer.findVehicle("X199CB99");
        } catch (VehicleNotFoundException e) {
            e.printStackTrace();
        } catch (InvalidNumberPlateException e) {
            e.printStackTrace();
        }

        try {
            customer.removeVehicle(vehicle);
        } catch (IllegalVehicleException e) {
            e.printStackTrace();
        } catch (VehicleNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static Workshop createWorkshop() {
        Workshop workshop = new Workshop();

        Customer customerSergeev = createCustomerSergeev();

        Customer customerPetrov = createCustomerPetrov();

        try {
            workshop.addCustomer(customerPetrov);
            workshop.addCustomer(customerSergeev);
        } catch (IllegalCustomerException e) {
            e.printStackTrace();
        } catch (CustomerAlreadyExistsException e) {
            e.printStackTrace();
        }

        return workshop;
    }

    private static Customer createCustomerPetrov() {
        Customer customerPetrov = new Customer("Mark", "Petrov", "+78005678930", "02.05.1981");
        Vehicle vehiclePetrovAudi = new Vehicle("A102AP199", "Audi", "A6", "blue");
        Vehicle vehiclePetrovMercedes = new Vehicle("X199CB99", "Mercedes", "SLR", "black");
        Vehicle vehiclePetrovKia = new Vehicle("H011AA199", "Kia", "Cerato", "white");
        Vehicle vehiclePetrovFiat = new Vehicle("H299CX199", "Fiat", "Punto", "black");

        try {
            customerPetrov.addVehicle(vehiclePetrovAudi);
            customerPetrov.addVehicle(vehiclePetrovFiat);
            customerPetrov.addVehicle(vehiclePetrovKia);
            customerPetrov.addVehicle(vehiclePetrovMercedes);
        } catch (VehicleAlreadyAssignedException e) {
            e.printStackTrace();
        } catch (IllegalVehicleException e) {
            e.printStackTrace();
        }
        return customerPetrov;
    }

    private static Customer createCustomerSergeev() {
        Customer customerSergeev = new Customer("Ivan", "Sergeev", "+38095234567", "13.02.1972");
        Vehicle vehicleSergeevRenault = new Vehicle("H174AK199", "Renault", "Logan", "red");
        Vehicle vehicleSergeevMercedes = new Vehicle("B099CC199", "Mercedes", "C200", "black");

        try {
            customerSergeev.addVehicle(vehicleSergeevMercedes);
            customerSergeev.addVehicle(vehicleSergeevRenault);
        } catch (VehicleAlreadyAssignedException e) {
            e.printStackTrace();
        } catch (IllegalVehicleException e) {
            e.printStackTrace();
        }
        return customerSergeev;
    }
}