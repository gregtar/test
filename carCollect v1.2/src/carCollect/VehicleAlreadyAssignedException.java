package carCollect;

public class VehicleAlreadyAssignedException extends Exception {
    public VehicleAlreadyAssignedException(String message) {
        super(message);
    }
 }