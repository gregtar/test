package carCollect;

public class InvalidNumberPlateException extends Exception {
    public InvalidNumberPlateException(String message) {
        super(message);
    }
}